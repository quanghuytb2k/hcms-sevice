<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->group(['prefix' => 'v1'], function () use ($router) {
    $router->group(['prefix' => 'companies'], function () use ($router) {
        $router->get('/data', 'V1\CompaniesController@data');
    });
    $router->group(['prefix' => 'dang_ky', 'middleware' => 'client'], function () use ($router) {
        $router->get('/dataCustomer', 'V1\FormsController@dataCustomer');
        $router->post('/saveAdult', 'V1\FormsController@saveAdult');
        $router->delete('/remove', 'V1\FormsController@remove');
        $router->put('/edit', 'V1\FormsController@edit');
        $router->post('/saveChild', 'V1\FormsController@saveChild');
        $router->post('/saveSecondForm', 'V1\FormsController@saveSecondForm');
        $router->get('/data', 'V1\FormsController@data');
        $router->get('/create', 'V1\FormsController@create');
    });
    $router->group(['prefix' => 'forms', 'middleware' => 'client'], function () use ($router) {
        $router->get('/index', 'V1\FormsController@index');
        $router->get('/edit', 'V1\FormsController@FormsEdit');
        $router->get('/remove', 'V1\FormsController@remove');
        $router->post('/save', 'V1\FormsController@FormsSave');
    });
});
