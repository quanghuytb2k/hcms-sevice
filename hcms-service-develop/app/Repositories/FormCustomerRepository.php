<?php

namespace App\Repositories;

use App\Models\FormCustomer;

class FormCustomerRepository extends BaseRepository
{
    protected $model;

    public function __construct(FormCustomer $model)
    {
        $this->model = $model;
    }

}
