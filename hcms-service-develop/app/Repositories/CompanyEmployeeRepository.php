<?php

namespace App\Repositories;

use App\Models\CompanyEmployee;

class CompanyEmployeeRepository extends BaseRepository
{
    protected $model;

    public function __construct(CompanyEmployee $model)
    {
        $this->model = $model;
    }

}
