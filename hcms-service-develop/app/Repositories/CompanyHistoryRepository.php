<?php

namespace App\Repositories;

use App\Models\CompanyHistory;

class CompanyHistoryRepository extends BaseRepository
{
    protected $model;

    public function __construct(CompanyHistory $model)
    {
        $this->model = $model;
    }

}
