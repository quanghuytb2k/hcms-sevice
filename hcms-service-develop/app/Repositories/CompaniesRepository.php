<?php

namespace App\Repositories;
use App\Models\Company;

class CompaniesRepository extends BaseRepository
{
    protected $model;

    public function __construct(Company $model)
    {
        $this->model = $model;
    }

}
