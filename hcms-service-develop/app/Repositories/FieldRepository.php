<?php

namespace App\Repositories;


use App\Models\Field;

class FieldRepository extends BaseRepository
{
    protected $model;

    public function __construct(Field $model)
    {
        $this->model = $model;
    }

}
