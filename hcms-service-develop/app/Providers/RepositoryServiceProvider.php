<?php

namespace App\Providers;

use App\Repositories\FormRepository;
use App\Services\FormServiceInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register Services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FormServiceInterface::class, FormRepository::class);
    }

    /**
     * Bootstrap Services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
