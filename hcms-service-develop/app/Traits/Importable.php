<?php

namespace App\Traits;

use App\Models\HealthRecord;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

trait Importable
{
    public function importXlsx($inputFileName, $clientExtension = 'xlsx')
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $readerXls = new \PhpOffice\PhpSpreadsheet\Reader\Xls();

        /** Load $inputFileName to a Spreadsheet Object  */

        //        $inputFileType = IOFactory::identify($inputFileName);
        //      $spreadsheet = $reader->load($inputFileType);
        /**  Create a new Reader of the type defined in $inputFileType  */
        ///$reader = IOFactory::createReader('xlsx');
        /**  Load $inputFileName to a Spreadsheet Object  */
        if ($clientExtension === 'xls') {
            $spreadsheet = $readerXls->load($inputFileName);
        } else {
            $spreadsheet = $reader->load($inputFileName);
        }
        $result = [];
        $sheet = $spreadsheet->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        //  Loop through each row of the worksheet in turn
        for ($row = 1; $row <= $highestRow; ++$row) {
            // set max row will break because memory overflow
            if ($row >= 10000) {
                break;
            }

            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray(
                'A' . $row . ':' . $highestColumn . $row,
                null,
                true,
                false
            );
            //  Insert row data array into your database of choice here

            $result[] = $rowData[0];
        }

        return $result;
    }

    public function importExcel($inputFileName, $clientExtension = 'xlsx')
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $readerXls = new \PhpOffice\PhpSpreadsheet\Reader\Xls();

        /** Load $inputFileName to a Spreadsheet Object  */

        //        $inputFileType = IOFactory::identify($inputFileName);
        //      $spreadsheet = $reader->load($inputFileType);
        /**  Create a new Reader of the type defined in $inputFileType  */
        ///$reader = IOFactory::createReader('xlsx');
        /**  Load $inputFileName to a Spreadsheet Object  */
        $worksheetNames = [];
        if ($clientExtension === 'xls') {
            $spreadsheet = $readerXls->load($inputFileName);
            $worksheetNames = $readerXls->listWorksheetNames($inputFileName);
        } else {
            $spreadsheet = $reader->load($inputFileName);
            $worksheetNames = $reader->listWorksheetNames($inputFileName);
        }
        $result = [];
        // $sheetCount = $spreadsheet->getSheetCount();
        $sheetNames = ['ominext'];
        if (count($worksheetNames) > 1) {
            $sheetNames = ['ominext', 'omi medical solution', 'omi medical solutions', 'omicare'];
        }
        // if ($sheetCount == 1) {
        //     $sheetName = ['OMINEXT'];
        // } else {
        //     $sheetName = ['OMINEXT', 'Omi Medical Solutions', 'OMICARE'];
        // }
        // if invalid sheet then return     
        if (!$spreadsheet) {
            return [];
        }
        $workings = [];
        foreach ($worksheetNames as $sheetName) {
            if (in_array(strtolower($sheetName), $sheetNames, false)) {
                $workings[] = $sheetName;
            }
        }
        // if not exist match sheet then return
        if (count($workings) == 0) return [];

        foreach ($workings as $item) {
            $sheet = $spreadsheet->getSheetByName($item);
            if (!$sheet) continue;
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            //  Loop through each row of the worksheet in turn
            for ($row = 4; $row <= $highestRow; ++$row) {
                // set max row will break because memory overflow
                if ($row >= 10000) {
                    break;
                }
                //  Read a row of data into an array
                $rowData = $sheet->rangeToArray(
                    'A' . $row . ':' . $highestColumn . $row,
                    null,
                    true,
                    false
                );
                //  Insert row data array into your database of choice here

                $result[] = $rowData[0];
            }
        }
        return $result;
    }
}
