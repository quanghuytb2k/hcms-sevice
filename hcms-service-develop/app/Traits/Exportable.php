<?php

namespace App\Traits;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

trait Exportable
{
    protected function exportHTML($entries)
    {
        $view = view('exports.index', [
            'toExports' => $this->toExports,
            'entries' => $entries,
        ])->toHtml();
        echo $view;

        die;
    }

    protected function exportXlsx($entries)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        foreach ($this->toExports as $key => $v) {
            if (is_string($v)) {
                $sheet->setCellValue($v.'1', $key);
            } elseif (is_array($v)) {
                list($c, $n) = $v;
                $sheet->setCellValue($c.'1', $n);
            }
        }

        foreach ($entries as $index => $entry) {
            $idx = $index + 2;

            if (!is_array($entry)) {
                $entry = $entry->toArray();
            }

            foreach ($this->toExports as $key => $v) {
                if (is_string($v)) {
                    $sheet->setCellValue("{$v}{$idx}", data_get($entry, $key));
                } elseif (is_array($v)) {
                    list($c, $n) = $v;
                    $sheet->setCellValue("{$c}{$idx}", data_get($entry, $key));
                }
            }
        }
        $writer = new Xlsx($spreadsheet);
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        $filename = uniqid().'-'.date('Y_m_d H_i').'.xlsx';

        // It will be called file.xls
        header('Content-Disposition: attachment; filename="'.$filename.'"');

        // Write file to the browser
        $writer->save('php://output');

        die;
    }
}
