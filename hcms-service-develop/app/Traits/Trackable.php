<?php

namespace App\Traits;

use App\Models\User;
use App\Models\ZLog;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait Trackable.
 *
 * @property ZLog $zLog
 */
trait Trackable
{
    private static $keepDay = 30;

    public static function boot()
    {
        parent::boot();
        /**
         * @var User $user
         */
        $user = auth()->user();

        if (!$user) {
            return;
        }

        static::created(function (Model $model) use ($user) {
            if (!ZLog::$enabled) {
                return;
            }
            $log = new ZLog();
            $log->table_id = $model->id;
            $log->conn = $model->getConnectionName();
            $log->action = 'INSERT';
            $log->table = $model->table;
            $log->origin_data = json_encode($model->toArray(), 128);
            $log->updated_by = $user->email;
            $log->keep_to = date('Y-m-d H:i:s', strtotime('+ '.self::$keepDay.' days'));
            $log->save();
        });

        static::updated(function (Model $model) use ($user) {
            if (!ZLog::$enabled) {
                return;
            }
            $log = new ZLog();
            $log->table_id = $model->id;
            $log->action = 'UPDATE';
            $log->conn = $model->getConnectionName();
            $log->table = $model->table;
            $log->updated_by = $user->email;
            $log->keep_to = date('Y-m-d H:i:s', strtotime('+ '.self::$keepDay.' days'));
            $log->setDiff($model->getChanges(), $model->getOriginal());

            $log->save();
        });

        static::deleted(function ($model) use ($user) {
            if (!ZLog::$enabled) {
                return;
            }
            $log = new ZLog();
            $log->action = 'DELETE';
            $log->conn = $model->getConnectionName();
            $log->table_id = $model->id;
            $log->table = $model->table;
            $log->keep_to = date('Y-m-d H:i:s', strtotime('+ '.self::$keepDay.' days'));
            $log->origin_data = json_encode($model->toArray(), 128);
            $log->updated_by = $user->email;
            $log->save();
        });
    }
}
