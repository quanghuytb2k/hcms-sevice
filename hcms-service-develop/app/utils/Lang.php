<?php
namespace App\utils;
class Lang
{
    const  MESSAGES = [
        'vi' => [
            "required_choose"=> "Vui lòng nhập/ chọn trường này",
            "added" => "Đã thêm",
            "regex_mes"=> "Chưa đúng định dạng",
            "not_regex"=> "Không bao gồm chữ và kí tự đặc biệt",
            "min_mes"=> "Tối thiểu 10 kí tự",
            "unique_mes"=> "Số điện thoại đã tồn tại trên hệ thống",
            "deleted"=> "Đã xóa",
            'required' => 'Vui lòng nhập trường này!',
            'not_found'=>"Không tìm thấy",
            "not_info"=> "Không có thông tin khách hàng",
            'otherGender' => 'Khác',
            'female' => 'Nữ',
            'male' => 'Nam',
            'married' => 'Đã kết hôn',
            'single' => 'Độc thân',
            'bao_hiem' => 'Bảo hiểm',
            'money' => 'Tiền mặt/ Thẻ',
            'company' => 'Công ty',
            'notFoundData' => 'Không tìm thấy kết quả',
            'updatedAt' => 'Ngày cập nhật',
            'updated' => 'Đã cập nhật',
        ],
        'en' => [
        ],
        'ja' => [

        ]
    ];
}
