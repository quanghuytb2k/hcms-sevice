<?php

function makeResponse($code,$error,$message,$data){
    return [
        'code' => $code,
        'error' => $error,
        'message' => $message,
        'data' => $data
    ];
}
