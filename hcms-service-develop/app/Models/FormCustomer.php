<?php

namespace App\Models;

use App\Traits\Trackable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int       $id
 * @property string    $code
 * @property string    $ma_benh_nhan
 * @property \DateTime $birth_date
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property \DateTime $created_by
 * @property \DateTime $updated_by
 * @property \DateTime $next_remind_at
 * @property int       $status
 * @property string    $summary
 * @property int       $customer_id
 * @property int       $omi_id
 */
class FormCustomer extends BaseModel
{
    //use Trackable;
    use SoftDeletes;

    protected $table = 'form_customers';
    protected $casts = [
    ];

    protected $fillable = [
        'ma_benh_nhan',
        'form_id', 'nguyen_vong_khac', 'biet_phong_kham', 'is_thuoc_huyet_ap', 'thuoc', 'benh_su_gia_dinh',
        'phau_thuat', 'benh_su', 'nhom_mau', 'can_nang', 'is_benh_bam_sinh', 'is_bat_thuong', 'benh_bam_sinh',
        'bat_thuong', 'is_truyen_mau', 'benh_su_gd_khac', 'nguon_tt_khac', 'vacxins', 'di_ung_thuoc', 'truyen_mau',
        'benh_luu_hanh', 'benh_su_khac'
    ];

//    public function contacts()
//    {
//        return $this->hasMany(Contact::class, 'customer_id');
//    }
//
//    public function getAvatar(): string
//    {
//        /*  if (!$this->avatar) {
//              $this->avatar = 'https://avatar.omikenko.vn/' . urlencode($this->name);
//              $this->save();
//          }*/
//
//        return config('app.avatar_url').'/'.urlencode($this->name);
//    }
//
//    public function source()
//    {
//        return $this->belongsTo(Source::class, 'source_id');
//    }
//
//    public function reminders()
//    {
//        return $this->hasMany(Reminder::class, 'customer_id', 'id');
//    }
//
//    public function formItems()
//    {
//        return $this->hasMany(FormItem::class, 'ma_benh_nhan', 'ma_benh_nhan');
//    }

    public static function withMeta()
    {
        return static::query()->with(['source',
            'customerHistories' => function ($q) {
                $q->orderBy('id', 'desc');
            }, 'contacts' => function ($q) {
                $q->with(['province', 'district']);
            }]);
    }

    public static function calculateAge($dob)
    {
        if (!$dob) {
            return '';
        }

        $dob = date('Y-m-d', strtotime($dob));

        $dobObject = new \DateTime($dob);
        $nowObject = new \DateTime();

        $diff = $dobObject->diff($nowObject);

        return $diff->y;
    }
}
