<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int       $id
 * @property string    $name
 * @property int       $company_id
 * @property int       $year
 * @property int       $employees
 * @property int       $tested_employees
 * @property string    $info
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property string    $created_by
 * @property string    $updated_by
 */
class CompanyEmployee extends BaseModel
{
    use SoftDeletes;

    protected $table = 'company_employees';
    protected $fillable = [
        'company_id',
        'year',
        'employees',
        'tested_employees'
    ];

    public function companyHealthRecords()
    {
        return $this->hasMany(HealthRecord::class, 'company_employee_id', 'id');
    }
}
