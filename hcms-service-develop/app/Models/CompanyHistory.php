<?php

namespace App\Models;

/**
 * @property int       $id
 * @property int       $company_id
 * @property int       $company_employees_id
 * @property array     $diff
 * @property string    $content
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property string    $created_by
 */
class CompanyHistory extends BaseModel
{
    protected $table = 'company_histories';
    protected $casts = [
        'diff' => 'array'
    ];

    public function translateDiff()
    {
        $provinceMap = Province::get()->pluck('name', 'id');
        $districtMap = District::get()->pluck('name', 'id');

        if (is_array($this->diff)) {
            $contents = [];

            foreach ($this->diff as $field => $changes) {
                if (!isset($changes[0])) {
                    $changes[0] = null;
                }

                if (!isset($changes[1])) {
                    $changes[1] = null;
                }

                if ($changes[0] == $changes[1]) {
                    continue;
                }

                if ($field === 'province_id') {
                    $changes[0] = $provinceMap[$changes[0]];
                    $changes[1] = $provinceMap[$changes[1]];
                } elseif ($field === 'district_id') {
                    $changes[0] = $districtMap($changes[0]);
                    $changes[1] = $districtMap($changes[1]);
                }

                if (!isset($this->company_employees_id)) {
                    $contents[] = sprintf(
                        '%s: %s → %s',
                        __('companies.'.$field),
                        __($changes[0]),
                        __($changes[1])
                    );
                } else {
                    if (isset($this->year) && $field === 'employees') {
                        $contents[] = sprintf(
                            'Thay đổi thông tin nhân sự khám: Năm khám: %s: Số nhân sự: %s → %s',
                            $this->year,
                            $changes[0],
                            $changes[1]
                        );
                    } else {
                        $contents[] = sprintf(
                            'Thay đổi thông tin khám:  %s: %s → %s',
                            __('companies.'.$field),
                            __($changes[0]),
                            __($changes[1])
                        );
                    }
                }
            }

            $this->contents = $contents;
            unset($this->diff);
        }
    }
}
