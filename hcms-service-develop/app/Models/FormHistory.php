<?php

namespace App\Models;

/**
 * @property int       $id
 * @property int       $form_id
 * @property array     $diff
 * @property string    $content
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property string    $created_by
 */
class FormHistory extends BaseModel
{
    public static $genderMap = [
        1 => 'Nam',
        2 => 'Nữ',
        3 => 'Khác'
    ];
    public static $paymentMethod = [
        1 => 'Bảo hiểm',
        2 => 'Tiền mặt/ Thẻ',
        3 => 'Công ty'
    ];
    public static $paymentAddress = [
        1 => 'Địa chỉ công ty',
        2 => 'Địa chỉ nhà riêng',
    ];
    protected $table = 'form_histories';
    protected $casts = [
        'diff' => 'array'
    ];

    public function translateDiff()
    {
        $provinceMap = Province::get()->pluck('name', 'id');
        $districtMap = District::get()->pluck('name', 'id');
        $genderMap = self::$genderMap;
        $paymentMap = self::$paymentMethod;
        $paymentAddress = self::$paymentAddress;

        if (is_array($this->diff)) {
            $contents = [];

            foreach ($this->diff as $field => $changes) {
                if (!isset($changes[0])) {
                    $changes[0] = null;
                }

                if (!isset($changes[1])) {
                    $changes[1] = null;
                }

                if ($changes[0] == $changes[1]) {
                    continue;
                }

                if ($field === 'province_id') {
                    $changes[0] = $changes[0] ? $provinceMap[$changes[0]] : '';
                    $changes[1] = $changes[1] ? $provinceMap[$changes[1]] : '';
                } elseif ($field === 'district_id') {
                    $changes[0] = $changes[0] ? $districtMap($changes[0]) : '';
                    $changes[1] = $changes[1] ? $districtMap($changes[1]) : '';
                }

                if ($field === 'sex') {
                    $changes[0] = $changes[0] ? $genderMap[$changes[0]] : '';
                    $changes[1] = $changes[1] ? $genderMap[$changes[1]] : '';
                }

                if ($field === 'payment_method') {
                    $changes[0] = $changes[0] ? $paymentMap[$changes[0]] : '';
                    $changes[1] = $changes[1] ? $paymentMap[$changes[1]] : '';
                }

                if ($field === 'payment_address') {
                    $changes[0] = $changes[0] ? $paymentAddress[$changes[0]] : '';
                    $changes[1] = $changes[1] ? $paymentAddress[$changes[1]] : '';
                }

                if ($field === 'hoat_dong') {
                    $changes[0] = $changes[1] = '';
                    $contents[] = 'Thêm đăng ký khám định kỳ: Hoạt động hàng ngày';
                }

                if ($field === 'thoi_quen') {
                    $changes[0] = $changes[1] = '';
                    $contents[] = 'Thêm đăng ký khám định kỳ: Thói quen sinh hoạt';
                }

                if (!isset($this->contact_id)) {
                    if ($field !== 'hoat_dong' && $field !== 'thoi_quen') {
                        $contents[] = sprintf(
                            '%s: %s → %s',
                            __('forms.'.$field),
                            ($changes[0]),
                            ($changes[1])
                        );
                    }
                } else {
                    $contents[] = sprintf(
                        'Thay đổi liên hệ:  %s: %s → %s',
                        __('contacts.'.$field),
                        __($changes[0]),
                        __($changes[1])
                    );
                }
            }

            $this->contents = $contents;
            unset($this->diff);
        }
    }
}
