<?php

namespace App\Models;

use App\Traits\Trackable;

/**
 * @property int       $id
 * @property string    $name
 * @property string    $color
 * @property int       $status
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property string    $type
 */
class Source extends BaseModel
{
    use Trackable;
    protected $table = 'sources';

    protected $fillable = [
        'name', 'color', 'type', 'status'
    ];

    public static function getSourceFromType($type)
    {
        return self::query()
            ->select('id', 'name')
            ->where('type', $type)
            ->where('status', 1)
            ->orderBy('name')
            ->get();
    }
}
