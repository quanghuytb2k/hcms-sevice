<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int       $id
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 */
class Form extends BaseModel
{
    use SoftDeletes;

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    const GENDER_OTHER = 3;

    const TYPE_UONG_RIEU_0 = 0;
    const TYPE_UONG_RIEU_1 = 1;
    const TYPE_UONG_RIEU_2 = 2;
    const TYPE_UONG_RIEU_3 = 3;
    const TYPE_UONG_RIEU_4 = 4;

    const TYPE_CAFE_0 = 0;
    const TYPE_CAFE_1 = 1;
    const TYPE_CAFE_2 = 2;

    public static $listTypeUongRieu = [
        self::TYPE_UONG_RIEU_0 => [
            'label' => 'Không uống',
            'point' => 1,
        ],
        self::TYPE_UONG_RIEU_1 => [
            'label' => 'Trước đây đã từng uống',
            'point' => 1,
        ],
        self::TYPE_UONG_RIEU_2 => [
            'label' => 'Uống (dưới 150g/tuần)',
            'point' => 0,
        ],
        self::TYPE_UONG_RIEU_3 => [
            'label' => 'Uống (trên 150g/tuần và dưới 450g/tuần)',
            'point' => 0,
        ],
        self::TYPE_UONG_RIEU_4 => [
            'label' => 'Uống (trên 450g/tuần)',
            'point' => 2,
        ]
    ];

    public static $listTypeCafe = [
        self::TYPE_CAFE_0 => [
            'label' => 'Không uống',
            'point' => 0,
        ],
        self::TYPE_CAFE_1 => [
            'label' => 'Không uống hàng ngày nhưng trên 1 lần/tuần',
            'point' => 0,
        ],
        self::TYPE_CAFE_2 => [
            'label' => 'Trên 1 cốc mỗi ngày',
            'point' => -1,
        ]
    ];
    protected $table = 'forms';
    protected $fillable = ['ma_benh_nhan', 'name', 'email', 'phone', 'address', 'birth_date', 'sex', 'tinh_trang_hon_nhan',
        'contact_array', 'thuoc', 'benh_su_gia_dinh', 'age', 'company', 'company_address', 'is_thuoc_huyet_ap',
        'cong_ty_bao_hiem', 'is_contact', 'is_di_ung', 'payment_address', 'nguyen_vong_khac', 'biet_phong_kham',
        'phau_thuat', 'benh_su', 'nhom_mau', 'can_nang', 'is_benh_bam_sinh', 'is_bat_thuong', 'benh_bam_sinh', 'bat_thuong',
        'is_truyen_mau', 'benh_su_gd_khac', 'nguon_tt_khac', 'payment_method', 'vacxins', 'hoat_dong', 'thoi_quen',
        'is_add_customer', 'benh_luu_hanh', 'is_bam_sinh', 'is_luu_hanh', 'school'];

    public function formHistories()
    {
        return $this->hasMany(FormHistory::class, 'form_id', 'id');
    }

    public function formItems()
    {
        return $this->hasMany(FormItem::class, 'form_id', 'id');
    }
}
