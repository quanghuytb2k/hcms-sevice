<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int       $id
 * @property string    $name
 * @property int       $employees
 * @property int       $employees_tested
 * @property string    $info
 * @property string    $address
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property string    $created_by
 * @property string    $updated_by
 */
class Company extends BaseModel
{
    use SoftDeletes;
    protected $table = 'companies';
    protected $fillable = [
        'name',
        'employees',
        'employees_tested',
        'address',
        'phone',
        'created_by',
        'updated_by',
    ];

    public function companyEmployees()
    {
        return $this->hasMany(CompanyEmployee::class, 'company_id', 'id');
    }

    public function companyHistories()
    {
        return $this->hasMany(CompanyHistory::class, 'company_id', 'id');
    }
}
