<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int       $id
 * @property int       $form_customer_id
 * @property string    $ma_benh_nhan
 * @property string    $hoat_dong
 * @property string    $thoi_quen
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property \DateTime $deleted_at
 */
class FormItem extends BaseModel
{
    use SoftDeletes;

    protected $table = 'form_items';
    protected $fillable = [
        'form_id',
        'ma_benh_nhan',
        'form_customer_id',
        'customer_id',
        'hoat_dong',
        'thoi_quen'
    ];

//    public function companyHealthRecords()
//    {
//        return $this->hasMany(HealthRecord::class, 'company_employee_id', 'id');
//    }

//    public static function boot()
//    {
//        parent::boot();
//        /**
//         * @var User $user
//         */
//        $user = auth_user();
//        $ignored = ['updated_at' => 1, 'created_by' => 1, 'created_at' => 1];
//
//        if (!$user) {
//            $user = new User();
//            $user->name = 'Bot';
//            $user->email = 'crm-bot@omicare.vn';
//        }
//
//        static::created(function (FormItem $formItem) use ($user) {
//            $log = new FormHistory();
//            $log->form_id = $formItem->form_id;
//            $log->diff = [];
//            $log->content = 'Có đăng ký khám định kỳ mới: Bệnh nhân: '.$formItem->ma_benh_nhan;
//            $log->created_by = $user->email;
//            $log->save();
//        });
//    }
}
