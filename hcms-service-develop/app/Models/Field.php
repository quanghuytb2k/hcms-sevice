<?php

namespace App\Models;

use App\Form\Types\ValidateRule;
use App\Traits\Trackable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int       $id
 * @property string    $field
 * @property string    $table
 * @property string    $name
 * @property string    $name_en
 * @property string    $name_ja
 * @property string    $default_value
 * @property string    $placeholder
 * @property string    $type
 * @property int       $order
 * @property string    $options
 * @property array     $rules
 * @property string    $attrs
 * @property string    $rounds
 * @property string    $daiAnToan
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property string    $column_type
 * @property int       $status
 * @property int       $show
 * @property int       $readonly
 * @property int       $processed
 * @property int       $order_form
 * @property int       $order_index
 * @property int       $order_filter
 * @property int       $filtered
 * @property int       $locked
 */
class Field extends BaseModel
{
    use SoftDeletes;
    use Trackable;

    public static $allowedTable = [
        'customers' => true,
        'orders' => true,
        'products' => true,
        'health_records' => true,
        'forms' => true
    ];

    protected $table = 'fields';
    protected $casts = [
        'readonly' => 'int',
        'locked' => 'int',
        'status' => 'int',
        'options' => 'array',
        'rules' => 'array',
        'safety_strip' => 'array',
        'attrs' => 'array',
    ];

    protected $fillable = [
        'name',
        'name_en',
        'name_ja',
        'placeholder',
        'type',
        'order',
        'options',
        'default_value',
        'field',
        'rules',
        'attrs',
        'rounds',
        'status',
        'group_id',
        'class',
        'style',
        'excel_name',
        'safety_strip',
        'unit'
    ];
}
