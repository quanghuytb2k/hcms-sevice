<?php

namespace App\Models;

use App\Models\OmiAccount\OAUser;
use App\Traits\Trackable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * @property int       $id
 * @property string    $code
 * @property string    $ma_benh_nhan
 * @property string    $first_name
 * @property string    $last_name
 * @property string    $name
 * @property string    $phone
 * @property string    $address
 * @property int       $gender
 * @property int       $sex
 * @property string    $email
 * @property \DateTime $birth_date
 * @property string    $language
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property \DateTime $created_by
 * @property \DateTime $updated_by
 * @property \DateTime $next_remind_at
 * @property int       $status
 * @property string    $summary
 * @property int       $level_id
 * @property int       $omi_id
 * @property string    $source_id
 * @property string    $tags
 * @property string    $trieu_chung
 * @property int       $follow_id
 * @property string    $attachments
 * @property string    $avatar
 * @property string    $level
 * @property string    $sliders
 * @property int       $point
 * @property int       $point_total
 * @property int       $point_in_year
 * @property int       $rank_id
 * @property int       $partner_id
 * @property \DateTime $archived_at
 * @property string    $nguy_co_tieu_duong
 * @property string    $nguy_co_dut_mach_mau
 * @property string    $nguy_co_ung_thu_gan
 */
class Customer extends BaseModel
{
    //use Trackable;
    use SoftDeletes;

    protected $table = 'customers';
    protected $casts = [
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'name',
        'sex',
        'ma_benh_nhan',
        'phone',
        'address',
        'gender',
        'birth_date',
        'language',
        'archived_at',
        'form_id',
        'cong_ty_bao_hiem',
        'payment_method',
        'email',
        'company_address',
        'nhom_mau',
        'tinh_trang_hon_nhan',
        'cong_ty_bao_hiem',
        'dia_chi_xuat_hoa_don',
        'omi_id',
        'password',
        'age',
        'payment_address',
        'phong_ban'
    ];

    protected $hidden = [
        'password'
    ];
    protected $dates = [];

    public function contacts()
    {
        return $this->hasMany(Contact::class, 'customer_id');
    }

    public function save(array $options = [])
    {
        $r = parent::save($options);

        if (!$this->code) {
            $this->code = 'KH'.$this->id;
            DB::update('UPDATE customers SET `code`=? WHERE id=?', [$this->code, $this->id]);
        }

        $changes = $this->getChanges();

        if (isset($changes['next_remind_at'])) {
            $this->storeRemind($changes['next_remind_at']);
        }

        return $r;
    }

    public function follower()
    {
        return $this->belongsTo(User::class, 'follow_id');
    }

    public function getAvatar(): string
    {
        /*  if (!$this->avatar) {
              $this->avatar = 'https://avatar.omikenko.vn/' . urlencode($this->name);
              $this->save();
          }*/

        return config('app.avatar_url').'/'.urlencode($this->name);
    }

    public function source()
    {
        return $this->belongsTo(Source::class, 'source_id');
    }

    public function customerHistories()
    {
        return $this->hasMany(CustomerHistory::class, 'customer_id', 'id');
    }

    public function reminders()
    {
        return $this->hasMany(Reminder::class, 'customer_id', 'id');
    }

    public function formItems()
    {
        return $this->hasMany(FormItem::class, 'ma_benh_nhan', 'ma_benh_nhan');
    }

    public function userOmiAccount()
    {
        return $this->hasOne(OAUser::class, 'id', 'omi_id');
    }

    public static function withMeta()
    {
        return static::query()->with(['source',
            'customerHistories' => function ($q) {
                $q->orderBy('id', 'desc');
            }, 'contacts' => function ($q) {
                $q->with(['province', 'district']);
            }]);
    }
}
