<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CompaniesController extends Controller
{
    protected $stressPartService;
    protected $stressQuestionService;
    protected $userStressService;

    public function __construct
    (
//        StressPartServiceInterface     $stressPartService,

    )
    {
//        $this->stressPartService = $stressPartService;
    }

//    public function index(Request $request)
//    {
//        $omi_id = $request->header('data');
//        $language = $request->header('Language') ?? StressQuestionRepository::LANGUAGE_VIETNAM;
//        $data = $this->userStressService->stressIndex($language, $omi_id);
//        return makeResponse(200, false, 'OK', $data);
//    }

    public function data(Request $req)
    {

        $years = CompanyEmployee::select('year')->groupBy('year')->get();
        $query = Company::query()->with(['companyHistories' => function ($q) {
            $q->orderBy('id', 'desc');
        }])
            ->with(['companyEmployees' => function ($q) use ($req) {
                if (isset($req->year)) {
                    $q->withCount('companyHealthRecords')->where('year', $req->year);
                }
                $q->withCount('companyHealthRecords')->orderBy('year', 'desc');
            }])
            ->orderBy('id', 'desc');
        if ($req->keyword) {
            $query->where('name', 'LIKE', '%'.$req->keyword.'%');
        }

        $entries = $query->paginate();

        return [
            'code' => 0,
            'data' => $entries->items(),
            'years' => $years,
            'paginate' => [
                'currentPage' => $entries->currentPage(),
                'lastPage' => $entries->lastPage(),
            ]
        ];
    }
}
