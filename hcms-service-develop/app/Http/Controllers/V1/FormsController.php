<?php

namespace App\Http\Controllers\v1;

use App\Models\Company;
use App\Services\FormServiceInterface;
use Illuminate\Http\Request;

class FormsController
{
    protected $formService;

    public function __construct
    (
        FormServiceInterface $formService
    )
    {
        $this->formService = $formService;
    }

    public function dataCustomer(Request $req)
    {
        $customerId = $req->ma_benh_nhan;
        $data = $this->formService->formDataCustomer($customerId);
        return makeResponse(200, false, 'OK',  $data );
    }

    public function saveAdult(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }
        $data = $req->get('customer');
        $response = $this->formService->formSaveAdult($data);
        return makeResponse(200, false, 'OK',  $response );

    }

    public function remove(Request $req)
    {
        $id = $req->id;
        $data = $this->formService->FormRemove($id);
        return makeResponse(200, false, 'OK',  $data );
    }

    public function edit(Request $req)
    {
        $id = $req->id;
        $data = $this->formService->FormEdit($id);
        return makeResponse(200, false, 'OK',  $data );
    }

    public function saveChild(Request $req)
    {
        $data = $req->get('customer');
        $response = $this->formService->FormsaveChild($data);
        return makeResponse(200, false, 'OK',  $response );

    }

    public function saveSecondForm(Request $req)
    {
        $data = $req->get('customer');
        $response = $this->formService->FormsaveSecondForm($data);
        return makeResponse(200, false, 'OK',  $response );
    }

    public function data(Request $req)
    {
        $customerId = $req->ma_benh_nhan;
        $data = $this->formService->Formdata($customerId);
        return makeResponse(200, false, 'OK',  $data );
    }

    public function create(Request $req)
    {
        $data = $this->formService->Formcreate();
        return makeResponse(200, false, 'OK',  $data );
    }

    public function index()
    {
        $data = $this->formService->FormsIndex();
        return makeResponse(200, false, 'OK',  $data );
    }

    public function FormsEdit(Request $req)
    {
        $id = $req->id;
        $data = $this->formService->FormsEdit($id);
        return makeResponse(200, false, 'OK',  $data );

    }

    public function FormsRemove(Request $req)
    {
        $id = $req->id;
        $data = $this->formService->FormsRemove($id);
        return makeResponse(200, false, 'OK',  $data );
    }

    public function FormsSave(Request $req)
    {
        $data = $req->get('entry');
        $response = $this->formService->FormsSave($data,$req->is_add_customer);
        return makeResponse(200, false, 'OK',  $response );
    }
}
