<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ClientMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next)
    {
        $secret = $request->header('Secret');
        if($secret !== config('app.client.secret')){
            return response([
                'code' => 500,
                'error' => true,
                'message' => 'Invalid client secret!'
            ]);
        }

        $response = $next($request);

        // Post-Middleware Action

        return $response;
    }
}
