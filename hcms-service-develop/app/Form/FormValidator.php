<?php

namespace App\Form;

use App\Form\Types\ValidateRule;
use App\Models\Field;
use Illuminate\Support\Facades\DB;

/**
 * Class FormValidator.
 *
 * @property Field $field
 */
class FormValidator
{
    private $currentRule;
    private $field;
    private $id;

    /**
     * @return ValidateRule[]
     */
    public static function getRules()
    {
        $rules = [
            [
                'rule' => 'unique', 'name' => 'Không trùng (unique)', 'args' => [['label' => 'Tên bảng']]
            ],
            ['rule' => 'required', 'name' => 'Bắt buộc', 'args' => []],
            ['rule' => 'min', 'name' => 'Giá trị nhỏ nhất (số)', 'args' => [
                ['label' => 'Giá trị nhỏ nhất']]
            ],
            ['rule' => 'max', 'name' => 'Giá trị lớn nhất (số)', 'args' => [
                ['label' => 'Giá trị lớn nhất']]
            ],
            ['rule' => 'minlength', 'name' => 'Độ dài tối thiểu(văn bản)', 'args' => [
                ['label' => 'Độ dài tối thiểu']]
            ],
            ['rule' => 'maxlength', 'name' => 'Độ dài tối đa(văn bản)', 'args' => [
                ['label' => 'Độ dài tối đa']]
            ],
            ['rule' => 'numeric', 'name' => 'Là số', 'args' => []],
            ['rule' => 'email', 'name' => 'Là email', 'args' => []],
            ['rule' => 'phone', 'name' => 'Là SĐT', 'args' => []],
            ['rule' => 'regex', 'name' => 'Regular Expression', 'args' => [
                ['label' => 'Nhập regex']]
            ],
        ];

        return ValidateRule::createFromArray($rules);
    }

    public function validate(Field $field, $value, $id = null): array
    {
        $this->field = $field;
        $this->id = $id;
        $rules = $field->getValidatorRules();

        $result = [];
        $otherRules = [];
        $hasValue = $this->verifyRequired($value);

        foreach ($rules as $rule) {
            if ($rule->rule === 'required') {
                if (!$hasValue) {
                    $result[] = $rule->message ?? __('rule.required');

                    return $result;
                }
            } else {
                if ($hasValue) {
                    $otherRules[] = $rule;
                }
            }
        }

        foreach ($otherRules as $rule) {
            $ruleAction = 'verify'.ucfirst($rule->rule);
            $args = array_column($rule->args, 'value');
            $defaultMessage = __('rule.'.$rule->rule);

            if (count($args)) {
                $defaultMessage = sprintf(__('rule.'.$rule->rule), ...$args);
            }

            $message = $rule->message ?? $defaultMessage;
            $this->currentRule = $rule;

            if (!$this->{$ruleAction}($value, ...$args)) {
                $result[] = $message;
            }
        }

        return $result;
    }

    public function verifyRequired($value = null): bool
    {
        return $value !== null && $value !== '';
    }

    public function verifyMin($value, $minValue): bool
    {
        return $value >= $minValue;
    }

    public function verifyMax($value, $maxValue): bool
    {
        return $value <= $maxValue;
    }

    public function verifyMinlength($value, $minlength): bool
    {
        return mb_strlen($value) >= $minlength;
    }

    public function verifyMaxlength($value, $maxlength): bool
    {
        return mb_strlen($value) <= $maxlength;
    }

    public function verifyNumeric($value): bool
    {
        return is_numeric($value);
    }

    public function verifyRegex($value, $pattern): bool
    {
        return preg_match($pattern, $value) === 1;
    }

    public function verifyEmail($value): bool
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    public function verifyPhone($value): bool
    {
        return preg_match('/^0(\d{9})$/', $value) === 1;
    }

    public function verifyUnique($value, $table): bool
    {
        if ($this->id) {
            return DB::table($table)
                ->where('id', '!=', $this->id)
                ->where($this->field->field, '=', $value)
                ->count() === 0;
        }

        return DB::table($table)
            ->where($this->field->field, '=', $value)
            ->count() === 0;
    }
}
