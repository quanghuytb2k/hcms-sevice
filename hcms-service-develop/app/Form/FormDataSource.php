<?php

namespace App\Form;

use App\Form\DataSources\DSCompany;
use App\Form\DataSources\DSCustomer;
use App\Form\DataSources\DSCustomerSource;
use App\Form\DataSources\DSManual;
use App\Form\DataSources\DSUser;
use App\Form\Types\DataSource;
use App\Models\Customer;
use Illuminate\Http\Request;

class FormDataSource
{
    /**
     * @return DataSource[]
     */
    public static function getDataSource(): array
    {
        $sources = [
            new DSCustomer(),
            new DSCustomerSource(),
            new DSUser(),
            new DSCompany(),
            new DSManual()
        ];

        return DataSource::createFromArray($sources);
    }

    /**
     * @return null|DataSource
     */
    public static function getSourceByValue(string $value)
    {
        $map = self::getDataSourceMap();

        if (!isset($map[$value])) {
            return null;
        }

        /**
         * @var DataSource $source
         */
        return $map[$value];
    }

    public static function getDataSourceMap(): array
    {
        static $map;

        if ($map) {
            return $map;
        }

        $map = [];
        $sources = self::getDataSource();

        foreach ($sources as $source) {
            if (isset($map[$source->value])) {
                throw new \Exception("Duplicate data source value: {$source['value']}");
            }

            $map[$source->value] = $source;
        }

        return $map;
    }

    public function get($value, Request $request)
    {
        $source = self::getSourceByValue($value);

        if (!$source) {
            return [];
        }

        return $source->get($request);
    }

    public static function getAll(): array
    {
        $entries = self::getDataSource();
        $result = [];
        $req = \request();

        foreach ($entries as $entry) {
            if ($entry->type === 'select') {
                $result[$entry->value] = $entry->get($req);
            }
        }

        return $result;
    }

    public function dataCustomers(Request $request)
    {
        $keyword = '%'.$request->get('query').'%';
        $value = $request->get('value');
        $query = Customer::selectRaw('id, name as label');

        if ($value) {
            $query->where('id', $value);

            return $query->limit(1)->get();
        }

        return $query->where('name', 'LIKE', $keyword)
            ->limit(100)
            ->get();
    }
}
