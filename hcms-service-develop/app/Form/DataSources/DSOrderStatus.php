<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\OrderStatus;

class DSOrderStatus extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Trạng Thái Đơn Hàng',
            'value' => 'order_status',
            'type' => 'select',
            'model' => OrderStatus::class,
            'select' => 'id,name,color',
        ]);
    }
}
