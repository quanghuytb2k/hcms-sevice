<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\Tag;

class DSCustomerTag extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Nhóm Khách Hàng',
            'value' => 'customer_tags',
            'type' => 'select',
            'model' => Tag::class,
            'where' => ['type' => 'customers'],
            'select' => 'id,name'
        ]);
    }
}
