<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\Company;

class DSCompany extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Công ty',
            'value' => 'company',
            'type' => 'select',
            'model' => Company::class,
            'select' => 'id,name'
        ]);
    }
}
