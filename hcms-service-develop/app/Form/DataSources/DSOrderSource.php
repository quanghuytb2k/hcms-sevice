<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\Source;

class DSOrderSource extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Nguồn Đơn Hàng',
            'value' => 'sources_orders',
            'type' => 'select',
            'model' => Source::class,
            'where' => ['type' => 'orders'],
            'select' => 'id,name,color'
        ]);
    }
}
