<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\Level;

class DSLevel extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Mối Quan Hệ',
            'value' => 'levels',
            'type' => 'select',
            'model' => Level::class,
            'select' => 'id,name,color'
        ]);
    }
}
