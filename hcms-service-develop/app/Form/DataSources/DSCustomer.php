<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\Customer;
use Illuminate\Database\Eloquent\Builder;

class DSCustomer extends DataSource
{
    protected $pageLimit = 25;

    public function __construct()
    {
        $input = [
            'name' => 'Khách Hàng(remote)',
            'value' => 'customers',
            'model' => Customer::class,
            'type' => 'ajax',
            'select' => 'id,name,email,phone,code,address',
            'formatValue' => [static::class, 'formatValue'],
            'buildQuery' => [static::class, 'buildQuery']
        ];

        parent::__construct($input);
    }

    public static function buildQuery(Builder $query, $request)
    {
        $values = $request['values'] ?? null;

        if ($values) {
            $values = explode(',', $values);
            $query->whereIn('id', $values);
        }

        if (isset($request['query'])) {
            $query->where(function (Builder $q) use ($request) {
                $keyword = '%'.$request['query'].'%';
                $q->where('name', 'LIKE', $keyword)
                    ->orWhere('phone', 'LIKE', $keyword)
                    ->orWhere('email', 'LIKE', $keyword)
                    ->orWhere('code', 'LIKE', $keyword);
            });
        }
    }

    public static function formatValue(Customer $value)
    {
        if (!$value->name) {
            return null;
        }

        $label = '['.$value->code.']'.$value->name;

        if ($value->phone) {
            $label .= ' - '.$value->phone;
        }

        if ($value->email) {
            $label .= ' - '.$value->email;
        }

        return [
            'id' => $value->id,
            'label' => $label,
            'name' => $value->name,
            'phone' => $value->phone,
            'email' => $value->email,
            'address' => $value->address
        ];
    }
}
