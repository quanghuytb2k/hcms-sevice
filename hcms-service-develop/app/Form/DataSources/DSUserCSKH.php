<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\Group;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Database\Eloquent\Builder;

class DSUserCSKH extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Chăm sóc KH',
            'value' => 'cskh',
            'type' => 'select',
            'model' => User::class,
            'select' => 'id,name',
            'buildQuery' => [static::class, 'buildQuery']
        ]);
    }

    public static function buildQuery(Builder $query, $request)
    {
        $ids = UserGroup::where('group_id', Group::CSKH)->get()->pluck('id', 'user_id')->toArray();
        $userIDs = array_keys($ids);
        $query->whereIn('id', $userIDs);
    }
}
