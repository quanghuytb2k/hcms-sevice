<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\Branch;

class DSBranch extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Chi nhánh',
            'value' => 'branch',
            'type' => 'select',
            'model' => Branch::class,
            'select' => 'id,name'
        ]);
    }
}
