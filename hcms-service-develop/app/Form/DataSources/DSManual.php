<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;

class DSManual extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Tự Định Nghĩa',
            'value' => 'manual',
            'method' => 'manual',
            'type' => 'manual'
        ]);
    }
}
