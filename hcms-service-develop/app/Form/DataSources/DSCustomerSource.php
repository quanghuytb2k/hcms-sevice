<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\Source;

class DSCustomerSource extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Nguồn Khách Hàng',
            'value' => 'sources_customer',
            'type' => 'select',
            'model' => Source::class,
            'where' => ['type' => 'customers'],
            'select' => 'id,name,name_ja,name_en,color'
        ]);
    }
}
