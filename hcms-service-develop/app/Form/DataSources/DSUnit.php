<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\Unit;

class DSUnit extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Đơn Vị Sản phẩm',
            'value' => 'units',
            'type' => 'select',
            'model' => Unit::class,
            'select' => 'id,name'
        ]);
    }
}
