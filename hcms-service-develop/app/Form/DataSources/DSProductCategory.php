<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\ProductCategory;

class DSProductCategory extends DataSource
{
    protected $pageLimit = 200;

    public function __construct()
    {
        parent::__construct([
            'name' => 'Nhóm Sản Phẩm',
            'value' => 'product_categories',
            'type' => 'select',
            'model' => ProductCategory::class,
            'select' => 'id,name'
        ]);
    }
}
