<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;

class DSProduct extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Sản Phẩm (remote)',
            'value' => 'products',
            'type' => 'ajax',
            'model' => Product::class,
            'select' => 'id,name,price,code,unit_id,quantity',
            'formatValue' => [static::class, 'formatValue'],
            'buildQuery' => [static::class, 'buildQuery']
        ]);
    }

    public static function buildQuery(Builder $query, $request)
    {
        $values = $request['values'] ?? null;

        if ($values) {
            $values = explode(',', $values);
            $query->whereIn('id', $values);
        }

        if (isset($request['query'])) {
            $query->where(function (Builder $q) use ($request) {
                $keyword = '%'.$request['query'].'%';
                $q->where('name', 'LIKE', $keyword)
                    ->orWhere('code', 'LIKE', $keyword);
            });
        }
    }

    public static function formatValue(Product $value)
    {
        if (!$value->name) {
            return null;
        }

        return [
            'id' => $value->id,
            'label' => $value->getNameWithPriceAndUnit(),
            'name' => $value->name,
            'unit_id' => (int) $value->unit_id,
            'price' => (int) $value->price,
            'quantity' => (int) $value->quantity,
        ];
    }
}
