<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\User;

class DSUser extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Người Phụ Trách',
            'value' => 'user',
            'type' => 'select',
            'model' => User::class,
            'select' => 'id,name'
        ]);
    }
}
