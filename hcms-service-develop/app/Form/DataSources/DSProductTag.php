<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\Tag;

class DSProductTag extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Nhóm Sản Phẩm',
            'value' => 'product_tags',
            'type' => 'select',
            'model' => Tag::class,
            'where' => ['type' => 'products'],
            'select' => 'id,name'
        ]);
    }
}
