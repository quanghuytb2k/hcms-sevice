<?php

namespace App\Form\DataSources;

use App\Form\Types\DataSource;
use App\Models\Source;

class DSFollowUpStatus extends DataSource
{
    public function __construct()
    {
        parent::__construct([
            'name' => 'Trạng thái chăm sóc',
            'value' => 'follow_up_status',
            'type' => 'select',
            'model' => Source::class,
            'where' => ['type' => 'follow_up'],
            'select' => 'id,name,name_ja,name_en,color'
        ]);
    }
}
