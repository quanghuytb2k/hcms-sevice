<?php

namespace App\Form;

class FormInput
{
    protected $type;
    protected $name;
    protected $rules = [];
    protected $attrs = [];
    protected $sqlType = 'TEXT';

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getRules(): array
    {
        return $this->rules;
    }

    public function getSqlType(): string
    {
        return $this->sqlType;
    }

    public function getAttrs(): array
    {
        return $this->attrs;
    }

    public static function getInputTypes(): array
    {
        return [
            'InputText',
            'InputTextarea',
            'InputNumber',
            'InputSelect',
            'InputQselect',
            'InputCheckbox',
            'InputUpload',
            'InputDate',
            'InputDatetime',
            'InputDaterange',
            'InputRichtext',
            'InputMeta',
            'InputThamdochucnang'
        ];
    }
}
