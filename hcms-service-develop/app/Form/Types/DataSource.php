<?php

namespace App\Form\Types;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class DataSource implements \JsonSerializable
{
    public $name;
    public $value;
    public $type;
    public $model;
    public $where;
    public $select;
    public $method;
    protected $buildQuery;
    protected $formatValue;
    protected $pageLimit = 50;

    private static $types = [
        'select' => true,
        'ajax' => true,
        'manual' => true
    ];

    public function __construct(array $data)
    {
        $this->name = $data['name'];
        $this->value = $data['value'];
        $this->type = $data['type'];

        if (!isset(self::$types[$this->type])) {
            throw new \Exception("Invalid DataSource Type: {$this->type}");
        }

        if ($this->value !== 'manual' && empty($data['model'])) {
            throw new \Exception("Missing datasource Model class for: {$this->value}");
        }

        $this->model = $data['model'] ?? null;
        $this->where = $data['where'] ?? [];
        $this->select = $data['select'] ?? '*';
        $this->method = $data['method'] ?? null;
        $this->buildQuery = $data['buildQuery'] ?? null;
        $this->formatValue = $data['formatValue'] ?? null;
    }

    /**
     * @return DataSource[]
     */
    public static function createFromArray(array $sources): array
    {
        $entries = [];
        $map = [];

        foreach ($sources as $source) {
            if (isset($map[$source->value])) {
                throw new \Exception("Value `{$source->value}` of source is duplicated");
            }

            if ($source instanceof DataSource) {
                $entries[] = $source;
                $map[$source->value] = true;
            } else {
                throw new \Exception('Invalid instance of DataSource');
            }
        }

        return $entries;
    }

    /**
     * @param array|Request $request
     *
     * @throws \Exception
     *
     * @return array| \Illuminate\Database\Eloquent\Collection
     */
    public function get($request = null)
    {
        if (!$request) {
            $request = [];
        }

        if (!$this->model) {
            return [];
        }

        $limit = $request['limit'] ?? $this->pageLimit;
        $query = $this->query();

        if (is_callable($this->buildQuery)) {
            call_user_func_array($this->buildQuery, [$query, $request]);
        } else {
            if (isset($request['query'])) {
                $query->where('name', 'LIKE', '%'.$request['query'].'%');
            }
        }

        $entries = $query->limit($limit)->get();

        if (is_callable($this->formatValue)) {
            $result = [];
            $formatValue = $this->formatValue;

            foreach ($entries as $entry) {
                $v = call_user_func_array($formatValue, [$entry]);

                if ($v !== null) {
                    $result[] = $v;
                }
            }

            return $result;
        }

        return $entries;
    }

    public function add(string $name): array
    {
        $model = $this->model;
        $query = $this->query()
            ->selectRaw($this->select)
            ->where($this->where);

        $entry = $query->where('name', $name)->first();

        if ($entry) {
            return [
                'code' => 404,
                'message' => "Giá Trị: {$name} đã tồn tại"
            ];
        }

        $instance = new $model();
        $instance->name = $name;

        foreach ($this->where as $k => $v) {
            $instance->{$k} = $v;
        }

        $instance->save();

        $data = $this->query()
            ->orderBy('id', 'desc')
            ->selectRaw($this->select)
            ->where($this->where)
            ->get();

        return [
            'code' => 200,
            'message' => 'Đã thêm',
            'data' => $data
        ];
    }

    public function toArray()
    {
        return [
            'name' => $this->name,
            'value' => $this->value,
            'type' => $this->type,
            'model' => $this->model,
            'where' => $this->where,
            'select' => $this->select,
            'method' => $this->method,
        ];
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function query(): Builder
    {
        if (!$this->model) {
            throw new \Exception('Missing datasource model');
        }

        return $this->model::query()
            ->selectRaw($this->select)
            ->where($this->where);
    }

    public function isAjax(): bool
    {
        return $this->type === 'ajax';
    }
}
