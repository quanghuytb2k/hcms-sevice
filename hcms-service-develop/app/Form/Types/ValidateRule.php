<?php

namespace App\Form\Types;

class ValidateRule implements \JsonSerializable
{
    public $rule;
    public $name;
    public $args;
    public $message;

    public function __construct(array $data)
    {
        $this->rule = $data['rule'];
        $this->name = $data['name'] ?? null;
        $this->args = $data['args'];
        $this->message = $data['message'] ?? null;
    }

    public static function createFromArray(array $rules): array
    {
        $result = [];

        foreach ($rules as $rule) {
            $r = new static($rule);
            $result[] = $r;
        }

        return $result;
    }

    public function jsonSerialize()
    {
        return [
            'rule' => $this->rule,
            'name' => $this->name,
            'args' => $this->args
        ];
    }
}
