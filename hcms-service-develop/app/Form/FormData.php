<?php

namespace App\Form;

use App\Helpers\NumberOperator;
use App\Models\BaseModel;
use App\Models\Customer;
use App\Models\Field;
use App\Models\FollowUp;
use App\Models\HealthRecord;
use App\Models\OmiAccount\OAUserRiskDiseases;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

class FormData
{
    public $id;
    public $data;

    public function __construct($id, array $data)
    {
        $this->id = $id;
        $this->data = $data;
    }

    public function save()
    {
        /**
         * @var Field $field
         */
        $data = $this->data;
        $id = $this->id;

        $fieldIDs = array_column($data, 'id');
        $fields = Field::whereIn('id', $fieldIDs)->get();
        $values = collect($data)->pluck('value', 'field')->toArray();
        $validator = new FormValidator();

        $errors = [];
        $dataGroup = [];

        $eval = new ExpressionLanguage();

        foreach ($fields as $field) {
            $value = $values[$field->field] ?? null;

            if ($value && is_numeric($value) && $round = $field->rounds) {
                $round = json_decode($round);

                if (!empty($round->type) && $round->type != '--') {
                    $str = 'n.' . $round->type . '(' . $value . ', ' . $round->precision . ')';

                    try {
                        $value = $eval->evaluate($str, ['n' => new NumberOperator()]);
                    } catch (\Exception $e) {
                        throw new \Exception($field->name . 'Không cho phép nhập kiểu text với những trường thiết lập làm tròn dữ liệu!');
                    }
                }
            }

            $field->value = $value;

            $dataGroup[$field->table][$field->field] = $value;

            $error = $validator->validate($field, $value, $id);

            if (!empty($error)) {
                $errors[] = [
                    'field' => $field->field,
                    'errors' => $error
                ];
            }
        }

        if (!empty($errors)) {
            return [
                'code' => 1,
                'errors' => $errors
            ];
        }

        $modelMap = [
            'customers' => Customer::class,
            'health_records' => HealthRecord::class
        ];

        if (count($dataGroup) !== 1) {
            throw new \Exception('Multiple table saving is not supported');
        }

        /**
         * @var BaseModel $model;
         */
        try {
            DB::beginTransaction();

            foreach ($dataGroup as $table => $dataToSave) {
                if (!isset($modelMap[$table])) {
                    throw new \Exception("Invalid table {$table}");
                }

                $class = $modelMap[$table];

                // validate unique code in health record
                if ($table == 'health_records') {
                    $rules = [
                        'code' => 'nullable|unique:'.$table.',code,'.$id,
                    ];

                    if ($dataToSave['type'] == HealthRecord::TYPE_MEDICINE) {
                        $rules['medical_service_id'] = 'required';
                    }
                    $messages = [
                        'code.unique' => 'Mã hồ sơ này đã tồn tại!',
                        'medical_service_id.required' => 'Chuyên khoa là bắt buộc phải chọn!',
                    ];

                    $v = Validator::make($dataToSave, $rules, $messages);

                    if ($v->fails()) {
                        $error = $v->errors()->first();

                        throw new \Exception($error ?? 'Có lỗi xảy ra, vui lòng tải lại trang và thử lại!');
                    }
                }

                $model = new $class();
                unset($dataToSave['id']);

                if (isset($id)) {
                    unset($dataToSave['id']);
                    $model = $model->find($id);
                }

                $model->forceFill($dataToSave);
                $model->save();

                if ($table == 'health_records') {
                    OAUserRiskDiseases::tinhNguyCoBenh($model);
                    FollowUp::createFollowUp($model->id);
                }

                $id = $model->id;
            }
            DB::commit();

            return [
                'code' => 200,
                'message' => 'Đã lưu',
                'id' => $id
            ];
        } catch (\Exception $e) {
            return [
                'code' => 2,
                'message' => $e->getMessage()
            ];
        }
    }
}
