<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

class InputDate extends FormInput
{
    protected $name = 'Chọn ngày';
    protected $type = 'date';
    protected $sqlType = 'DATE';
}
