<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

class InputText extends FormInput
{
    protected $name = 'Nhập Văn bản';
    protected $type = 'text';
    protected $sqlType = 'VARCHAR(300)';
}
