<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

class InputSelect extends FormInput
{
    protected $name = 'Lựa chọn';
    protected $type = 'select';
    protected $sqlType = 'VARCHAR(300)';

    protected $attrs = [
        [
            'name' => 'datasource', 'type' => 'datasource', 'value' => ''
        ]
    ];
}
