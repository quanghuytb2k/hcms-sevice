<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

class InputDatetime extends FormInput
{
    protected $name = 'Chọn ngày/giờ';
    protected $type = 'datetime';
    protected $sqlType = 'DATETIME';
}
