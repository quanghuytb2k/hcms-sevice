<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

/**
 * JSON_CONTAINS(JSON_EXTRACT(nhom_sp, '$[*].id'),'9', '$')
 * Class InputTreeselect.
 */
class InputTreeselect extends FormInput
{
    protected $name = 'TreeSelect';
    protected $type = 'treeselect';
    protected $sqlType = 'VARCHAR(300)';

    protected $attrs = [
        [
            'name' => 'multiple', 'type' => 'checkbox', 'value' => false,
        ],
        [
            'name' => 'async', 'type' => 'checkbox', 'value' => false
        ],
        [
            'name' => 'datasource', 'type' => 'datasource', 'value' => '',
        ],
    ];
}
