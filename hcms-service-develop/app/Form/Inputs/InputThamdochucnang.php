<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

class InputThamdochucnang extends FormInput
{
    protected $name = 'Đặc biệt';
    protected $type = 'Thamdochucnang';

    protected $attrs = [
        [
            'name' => 'datasource', 'type' => 'datasource', 'value' => '',
        ],
    ];
}
