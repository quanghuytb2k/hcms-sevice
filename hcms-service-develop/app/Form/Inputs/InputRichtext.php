<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

class InputRichtext extends FormInput
{
    protected $name = 'Trình soạn thảo';
    protected $type = 'richtext';
    protected $sqlType = 'TEXT';
}
