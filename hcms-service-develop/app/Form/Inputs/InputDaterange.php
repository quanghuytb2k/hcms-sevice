<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

class InputDaterange extends FormInput
{
    protected $name = 'Chọn khoảng thời gian';
    protected $type = 'daterange';
    protected $sqlType = 'VARCHAR(200)';
}
