<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

class InputMeta extends FormInput
{
    protected $name = 'Thuộc Tính Tự Do';
    protected $type = 'meta';
    protected $sqlType = 'DATETIME';
}
