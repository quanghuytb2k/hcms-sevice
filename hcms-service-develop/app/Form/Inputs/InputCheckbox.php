<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

class InputCheckbox extends FormInput
{
    protected $name = 'Checkbox';
    protected $type = 'checkbox';
    protected $sqlType = 'TINYINT(1)';
}
