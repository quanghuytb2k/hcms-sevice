<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

class InputQselect extends FormInput
{
    protected $name = 'Lựa chọn (mở rộng)';
    protected $type = 'qselect';

    protected $attrs = [
        [
            'name' => 'multiple', 'type' => 'checkbox', 'value' => false
        ],
        [
            'name' => 'quickadd', 'type' => 'checkbox', 'value' => false
        ],
        [
            'name' => 'datasource', 'type' => 'datasource', 'value' => '',
        ],
    ];
}
