<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

class InputNumber extends FormInput
{
    protected $name = 'Nhập số';
    protected $type = 'number';
    protected $sqlType = 'INT';
}
