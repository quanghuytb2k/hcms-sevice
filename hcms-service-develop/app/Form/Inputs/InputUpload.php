<?php

namespace App\Form\Inputs;

use App\Form\FormInput;

class InputUpload extends FormInput
{
    protected $name = 'Upload file';
    protected $type = 'upload';

    protected $attrs = [
        [
            'name' => 'multiple', 'type' => 'checkbox', 'value' => false,
        ],
        [
            'name' => 'photo', 'type' => 'checkbox', 'value' => false,
        ],
    ];
}
