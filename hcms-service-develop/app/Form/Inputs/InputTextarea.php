<?php

namespace App\Form\Inputs;

class InputTextarea extends InputText
{
    protected $name = 'Nhập văn bản (mở rộng)';
    protected $type = 'textarea';
    protected $sqlType = 'VARCHAR(500)';

    protected $attrs = [
        ['name' => 'height', 'type' => 'text', 'value' => '']
    ];
}
