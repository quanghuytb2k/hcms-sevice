<?php

namespace App\Services;
use App\Repositories\StressQuestionRepository;
use App\Repositories\UserStressRepository;
use App\Models\Post;

class CompaniesServiceImpl implements CompaniesServiceInterface
{
    public $stressQuestionRepository;
    public $stressPartServiceImpl;

    public function __construct(StressQuestionRepository $stressQuestionRepository, StressPartServiceImpl $stressPartServiceImpl)
    {
        $this->stressQuestionRepository = $stressQuestionRepository;
        $this->stressPartServiceImpl = $stressPartServiceImpl;
    }


}
