<?php

namespace App\Services;


use App\Form\FormDataSource;
use App\Models\Field;
use App\Repositories\FieldRepository;

class FieldServiceImpl implements FieldServiceInterface
{
    public $fieldRepository;

    public function __construct
    (
        FieldRepository $fieldRepository
    )
    {
        $this->fieldRepository = $fieldRepository;
    }
    public function getFieldsForTable(string $table, $show = null, $orderBy = 'order_form', $root = false)
    {
        if (!$show) {
            $show = [0, 1];
        }

        if (!is_array($show)) {
            $show = [$show];
        }

        /**
         * @var Field $field
         */
        $fields = $this->fieldRepository->findBy('table', $table)
            ->orderBy($orderBy, 'asc')
            ->where('status', 1)
            ->whereIn('show', $show)
            ->where('processed', 1)
            ->where(function ($q) use ($root) {
                if ($root) {
                    return $q->whereNull('group_id');
                }

                return true;
            })
            ->get();

        foreach ($fields as $field) {
            $attr = new \stdClass();

            if (is_array($field->attrs)) {
                foreach ($field->attrs as $a) {
                    $attr->{$a['name']} = $a['value'] ?? null;
                }
            }

            if (!is_array($field->options)) {
                $field->options = [];
            }

            unset($field->attrs);
            $field->attr = $attr;

            if ($field->type === 'qselect' || $field->type === 'select') {
                $datasource = $attr->datasource ?? null;

                if ($datasource !== 'manual') {
                    $source = FormDataSource::getSourceByValue($datasource);

                    if ($source) {
                        $field->options = $source->get();
                    }
                }
            }
        }

        return $fields;
    }

    public function getFilterForTable(string $table)
    {
        /**
         * @var Field $field
         */
        $fields = Field::where('table', $table)
            ->orderBy('order_filter', 'asc')
            ->where('status', 1)
            ->where('show', 1)
            ->where('filtered', 1)
            ->where('processed', 1)
            ->get();

        foreach ($fields as $field) {
            $attr = new \stdClass();

            if (is_array($field->attrs)) {
                foreach ($field->attrs as $a) {
                    $attr->{$a['name']} = $a['value'] ?? null;
                }
            }

            unset($field->attrs);
            $field->attr = $attr;

            if ($field->type === 'qselect' || $field->type === 'select') {
                $datasource = $attr->datasource ?? null;

                if ($datasource !== 'manual') {
                    $source = FormDataSource::getSourceByValue($datasource);

                    if ($source) {
                        $field->options = $source->get();
                    }
                }
            }
        }

        return $fields;
    }




}
