<?php

namespace App\Services;

use Illuminate\Http\Request;

interface FormServiceInterface
{
    public function formDataCustomer($customerId);
    public function formSaveAdult($data);
    public function FormRemove($id);
    public function FormEdit($id);
    public function FormsaveChild($data);
    public function FormsaveSecondForm($data);
    public function Formdata($customerId);
    public function Formcreate();
    public function FormsIndex();
    public function FormsEdit($id);
    public function FormsRemove($id);
    public function FormsSave($data, $is_add_customer);
}
