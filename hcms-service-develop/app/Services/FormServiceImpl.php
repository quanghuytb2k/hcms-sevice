<?php

namespace App\Services;

use App\Form\FormDataSource;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Form;
use App\Models\FormCustomer;
use App\Models\FormItem;
use App\Models\Province;
use App\Repositories\CustomerRepository;
use App\Repositories\FieldRepository;
use App\Repositories\FormCustomerRepository;
use App\Repositories\FormRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FormServiceImpl implements FormServiceInterface
{
    public $formRepository;
    public $formCustomerRepository;
    public $customerRepository;
    public $fieldRepository;
    public $fieldServiceImpl;

    public function __construct(
        FormRepository $formRepository,
        CustomerRepository $customerRepository,
        FormCustomerRepository $formCustomerRepository,
        FieldRepository $fieldRepository,
        FieldServiceImpl $fieldServiceImpl
    )
    {
        $this->formRepository = $formRepository;
        $this->customerRepository = $customerRepository;
        $this->formCustomerRepository = $formCustomerRepository;
        $this->fieldRepository = $fieldRepository;
        $this->fieldServiceImpl = $fieldServiceImpl;
    }

    public function formDataCustomer($customerId)
    {
        if (isset($customerId)) {
            $customer = $this->customerRepository->findBy('ma_benh_nhan',$customerId)->first();
            if ($customer) {
                $customer->toArray();
                return [
                    'data' => $customer,
                ];
            }
        }

        return [
            'code' => 2,
            'data' => [],
        ];
    }
    public function formSaveAdult($data)
    {
        $rules = [
            'name' => 'required',
            'phone' => 'required|regex:/(0)[0-9]/|not_regex:/[a-z]/|min:10',
            'sex' => 'required',
            'birth_date' => 'required',
        ];
        $messages = [
            'required' => $this->customerRepository->getMessageByLang('required_choose','Vui lòng nhập/ chọn trường này'),
            'regex' => $this->customerRepository->getMessageByLang('regex_mes','Chưa đúng định dạng'),
            'not_regex' => $this->customerRepository->getMessageByLang('not_regex','Không bao gồm chữ và kí tự đặc biệt'),
            'min' => $this->customerRepository->getMessageByLang('min_mes','Tối thiểu 10 kí tự'),
            'unique' => $this->customerRepository->getMessageByLang('unique_mes','Số điện thoại đã tồn tại trên hệ thống'),
        ];

        $v = Validator::make($data, $rules, $messages);

        if ($v->fails()) {
            return [
                'code' => 2,
                'errors' => $v->errors()
            ];
        }
        DB::beginTransaction();

        $arr = [
            "contact_array" => json_encode($data['contacts']),
            "di_ung_thuoc" => json_encode($data['diUngArray']),
            "phau_thuat" => json_encode($data['phauThuatArray']),
            "truyen_mau" => json_encode($data['truyenMauArray']),
            "thuoc" => json_encode($data['thuocArray']),
            "benh_su" => json_encode($data['benhSuArray']),
            "benh_su_gia_dinh" => json_encode($data['benhSuGiaDinh']),
            "biet_phong_kham" => json_encode($data['bietPhongKham']),
            "is_thuoc_huyet_ap" => $data['isThuocHuyetAp'],
            "nguyen_vong_khac" => $data['nguyenVongKhac'],
            "benh_su_khac" => $data['benhSuKhac'],
            "is_benh_su_khac" => $data['isBenhSuKhac'],
            "benh_su_gd_khac" => $data['benhSuGDKhac'],
            "nguon_tt_khac" => $data['nguonTTKhac'],
            "is_truyen_mau" => $data['isTruyenMau'],
            "ma_benh_nhan" => $data['ma_benh_nhan'],
        ];
        $entry = $this->formRepository->create($arr);
        $formCustomer = [
            "form_id" => $entry->id,
            "ma_benh_nhan" => $entry->ma_benh_nhan,
            "di_ung_thuoc" => $entry->di_ung_thuoc,
            "phau_thuat" => $entry->phau_thuat,
            "truyen_mau" => $entry->truyen_mau,
            "thuoc" => $entry->thuoc,
            "benh_su" => $entry->benh_su,
            "benh_su_gia_dinh" => $entry->benh_su_gia_dinh,
            "biet_phong_kham" => $entry->biet_phong_kham,
            "is_thuoc_huyet_ap" => $entry->is_thuoc_huyet_ap,
            "nguyen_vong_khac" => $entry->nguyen_vong_khac,
            "benh_su_khac" => $entry->benh_su_khac,
            "is_benh_su_khac" => $entry->is_benh_su_khac,
            "benh_su_gd_khac" => $entry->benh_su_gd_khac,
            "nguon_tt_khac" => $entry->nguon_tt_khac,
            "is_truyen_mau" => $entry->is_truyen_mau,
        ];

        $created = $this->formCustomerRepository->create($formCustomer);

        DB::commit();

        return [
            'code' => 0,
            'message' => $this->formCustomerRepository->getMessageByLang('added','Đã thêm'),
        ];
    }
    public function FormRemove($id){
        $entry = $this->formRepository->find($id);

        if (!$entry) {
            throw new NotFoundHttpException();
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => $this->customerRepository->getMessageByLang('deleted', 'Đã xóa'),
        ];
    }

    public function FormEdit($id){
        $entry = $this->formRepository->find($id);

        if (!$entry) {
            throw new NotFoundHttpException();
        }

        return [
            'code' => 200,
            'entry' => $entry
        ];
    }

    public function FormsaveChild($data){
        $rules = [
            'name' => 'required',
            'birth_date' => 'required',
            'sex' => 'required',
        ];

        $messages = [
            'required' =>$this->customerRepository->getMessageByLang('required_choose','Vui lòng nhập/ chọn trường này'),
        ];

        $v = Validator::make($data, $rules, $messages);

        if ($v->fails()) {
            return [
                'code' => 2,
                'errors' => $v->errors()
            ];
        }
        DB::beginTransaction();

        $entry = [
            "contact_array" => json_encode($data['contacts']),
            "benh_su" => json_encode($data['tienSuBenh']),
            "benh_su_khac" => $data['tienSuBenhKhac'],
            "bat_thuong" => json_encode($data['batThuongArr']),
            "benh_bam_sinh" => json_encode($data['benhBamSinhArr']),
            "benh_luu_hanh" => json_encode($data['benhLuuHanhArr']),
            "vacxins" => json_encode($data['vacxins']),
        ];

        $created = $this->formRepository->create($entry);
        DB::commit();

        return [
            'code' => 200,
            'message' => $this->formCustomerRepository->getMessageByLang('added','Đã thêm'),
        ];
    }

    public function FormsaveSecondForm($data)
    {
        $rules = [
            'name' => 'required',
            'phone' => 'required'
        ];
        $messages = [
            'required' => $this->formCustomerRepository->getMessageByLang('required','Vui lòng nhập trường này!'),
        ];

        $v = Validator::make($data, $rules, $messages);

        if ($v->fails()) {
            return [
                'code' => 2,
                'errors' => $v->errors()
            ];
        }

        DB::beginTransaction();

        $entry = $this->formRepository->findBy('ma_benh_nhan', $data['ma_benh_nhan'])
            ->orderBy('id', 'desc')
            ->first();

        if (!$entry) {
            $customer = $this->customerRepository->findBy('ma_benh_nhan', $data['ma_benh_nhan'])->first()->toArray();

            if ($customer) {
                $entry = new Form();
                $entry->fill($customer);
            } else {
                return [
                    'code' => 3,
                    'message' => $this->formCustomerRepository->getMessageByLang('not_found','Không tìm thấy'),
                ];
            }
        }

        $entry->fill($data);
        $entry->save();
        $formCustomer = $this->formCustomerRepository->findBy('ma_benh_nhan', $entry->ma_benh_nhan)
            ->orderBy('id', 'desc')
            ->first();

        $formItem = new FormItem();
        $formItem->form_id = $entry->id;
        $formItem->ma_benh_nhan = $entry->ma_benh_nhan;
        $formItem->form_customer_id = $formCustomer->id;

        $hoatDong = new \stdClass();
        $hoatDong->isTangCan = $data['isTangCan'];
        $hoatDong->soCanTang = $data['soCanTang'];
        $hoatDong->lanAnNgoai = $data['lanAnNgoai'];
        $hoatDong->soBuaNgay = $data['soBuaNgay'];
        $hoatDong->isTapTheDuc = $data['isTapTheDuc'];
        $hoatDong->tapTheDuc = $data['isTapTheDuc'] ? $data['tapTheDuc'] : '';

        $thoiQuen = new \stdClass();
        $thoiQuen->isHutThuoc = $data['isHutThuoc'];
        $thoiQuen->isRuouBia = $data['isRuouBia'];
        $thoiQuen->bia = $data['bia'] ? $data['bia'] : null;
        $thoiQuen->ruou = $data['ruou'] ? $data['ruou'] : null;
        $thoiQuen->cafe = $data['cafe'] ? $data['cafe'] : null;
        $thoiQuen->hutThuoc = $data['hutThuoc'] ? $data['hutThuoc'] : null;
        $thoiQuen->ruouBiaArr = json_encode(isset($data['ruouBiaArray']) ? $data['ruouBiaArray'] : []);
        $thoiQuen->daiTien = isset($data['isHangNgay']) ? $data['isHangNgay'] : false;
        $thoiQuen->soNgayDaiTien = $data['daiTien']['soNgay'];
        $thoiQuen->dungManHinh = $data['dungManHinh'];
        $thoiQuen->isDungMH = $data['isDungMH'];
        $thoiQuen->isDungMT = $data['isDungMT'];
        $thoiQuen->isNguNgon = $data['isNguNgon'];
        $thoiQuen->soGioNgu = $data['soGioNgu'];

        $hoatDongArr = $entry->hoat_dong ? json_decode($entry->hoat_dong, true) : [];
        $thoiQuenArr = $entry->thoi_quen ? json_decode($entry->thoi_quen, true) : [];

        array_push($hoatDongArr, $hoatDong);
        array_push($thoiQuenArr, $thoiQuen);

        $formItem->hoat_dong = json_encode($hoatDong);
        $formItem->thoi_quen = json_encode($thoiQuen);

        $entry->hoat_dong = json_encode($hoatDongArr);
        $entry->thoi_quen = json_encode($thoiQuenArr);

        $entry->save();
        $formItem->save();

        DB::commit();

        return [
            'code' => 200,
            'message' => $this->formCustomerRepository->getMessageByLang('added','Đã thêm'),
        ];
    }

    public function Formdata($customerId)
    {
        $customerForm = $this->formRepository->findBy('ma_benh_nhan',  $customerId)->first();

        if ($customerForm) {
            $customerForm->toArray();

            return [
                'code' => 200,
                'data' => $customerForm,
            ];
        }
        $customer = $this->customerRepository->findBy('ma_benh_nhan', $customerId)->first();

        if ($customer) {
            $customer->toArray();

            return [
                'code' => 200,
                'data' => $customer,
            ];
        }

        return [
            'code' => 1,
            'data' => [],
            'messages' => $this->formCustomerRepository->getMessageByLang('not_info', 'Không có thông tin khách hàng'),
        ];
    }

    public function Formcreate()
    {
        $entry = new Form();
        $entry->status = 1;
        $fields = $this->fieldServiceImpl->getFieldsForTable('forms', null, 'order_form');
        $customerFields = $this->fieldServiceImpl->getFieldsForTable('customers', null, 'order_form');
        $dataSourceMap = FormDataSource::getAll();
        $provinces = Province::get();
        return[
            "fields"=>$fields,
            "dataSourceMap"=>$dataSourceMap,
            "entry"=>$entry,
            "provinces"=>$provinces,
            "customerFields"=>$customerFields
        ];
    }

    public function FormsIndex()
    {
        $fields = $this->fieldServiceImpl->getFieldsForTable('forms', 1, 'order_index');
        $filterFields = $this->fieldServiceImpl->getFilterForTable('forms');
        $companies = Company::get();
        $genderMap = [
            1 =>$this->formCustomerRepository->getMessageByLang('male', 'Nam'),
            2 =>$this->formCustomerRepository->getMessageByLang('female', 'Nữ'),
            3 =>$this->formCustomerRepository->getMessageByLang('otherGender', 'Khác'),
        ];
        return[
            "fields"=>$fields,
            "filterFields"=>$filterFields,
            "companies"=>$companies,
            "genderMap"=>$genderMap
        ];
    }
    public function FormsEdit($id)
    {
        $omi_id = null;
        $entry = Form::with(['formHistories' => function ($q) {
            $q->orderBy('id', 'desc');
        }])->find($id);
        $marriedMap = [
            1 => $this->formCustomerRepository->getMessageByLang('single', 'Độc thân'),
            2 => $this->formCustomerRepository->getMessageByLang('married', 'Đã kết hôn'),
        ];
        $genderMap = [
            1 => $this->formCustomerRepository->getMessageByLang('male', 'Nam'),
            2 => $this->formCustomerRepository->getMessageByLang('female', 'Nữ'),
            3 => $this->formCustomerRepository->getMessageByLang('otherGender', 'Khác'),
        ];
        $paymentMethods = [
            1 => $this->formCustomerRepository->getMessageByLang('bao_hiem', 'Nữ'),
            2 => $this->formCustomerRepository->getMessageByLang('money', 'Tiền mặt/ Thẻ'),
            3 => $this->formCustomerRepository->getMessageByLang('company', 'Công ty'),
        ];

        $contactArray = json_decode($entry->contact_array, true);
        $thuocArray = json_decode($entry->thuoc, true);
        $diUngArray = json_decode($entry->di_ung_thuoc, true);
        $truyenMauArray = json_decode($entry->truyen_mau, true);
        $benhSuArray = json_decode($entry->benh_su, true);
        $phauThuatArray = json_decode($entry->phau_thuat, true);
        $nguonThongTin = json_decode($entry->biet_phong_kham, true);
        $benhSuGiaDinh = json_decode($entry->benh_su_gia_dinh, true);
        $vacxinArray = (array) json_decode($entry->vacxins, true);
        $hoatDongArray = $entry->hoat_dong ? json_decode($entry->hoat_dong, true) : [];
        $thoiQuenArray = $entry->thoi_quen ? json_decode($entry->thoi_quen, true) : [];
        $benhLuuHanhArray = $entry->benh_luu_hanh ? json_decode($entry->benh_luu_hanh, true) : [];
        $benhBamSinhArray = $entry->benh_bam_sinh ? json_decode($entry->benh_bam_sinh, true) : [];
        $batThuongArray = $entry->bat_thuong ? json_decode($entry->bat_thuong, true) : [];

        if (!$entry) {
            throw new NotFoundHttpException();
        }

        foreach ($entry->formHistories as $history) {
            $history->translateDiff();
        }

        if (isset($entry->ma_benh_nhan)) {
            $customer = $this->customerRepository->findBy('ma_benh_nhan',  $entry->ma_benh_nhan)->first();

            if ($customer) {
                $omi_id = $customer->omi_id;
            }
        }

        if (count($hoatDongArray) > 0) {
            foreach ($hoatDongArray as &$value) {
                $value['isShow'] = false;
            }
        }

        return[
            'entry'=>$entry,
            'contactArray'=>$contactArray,
            'thuocArray'=>$thuocArray,
            'diUngArray'=>$diUngArray,
            'truyenMauArray'=>$truyenMauArray,
            'benhSuArray'=>$benhSuArray,
            'phauThuatArray'=>$phauThuatArray,
            'nguonThongTin'=>$nguonThongTin,
            'benhSuGiaDinh'=>$benhSuGiaDinh,
            'hoatDongArray'=>$hoatDongArray,
            'thoiQuenArray'=>$thoiQuenArray,
            'vacxinArray'=>$vacxinArray,
            'omi_id'=>$omi_id,
            'benhLuuHanhArray'=>$benhLuuHanhArray,
            'benhBamSinhArray'=>$benhBamSinhArray,
            'batThuongArray'=>$batThuongArray,
            'marriedMap'=>$marriedMap,
            'genderMap'=>$genderMap,
            'paymentMethods'=>$paymentMethods,
        ];
    }

    public function FormsRemove($id)
    {
        $entry = $this->formRepository->find($id);

        if (!$entry) {
            throw new NotFoundHttpException();
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => $this->formCustomerRepository->getMessageByLang('deleted', 'Đã xóa'),
        ];
    }

    public function FormsSave($data, $is_add_customer)
    {
        $rules = [];

        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            return [
                'code' => 2,
                'errors' => $v->errors()
            ];
        }
        if (isset($data['id'])) {
            $entry = $this->formRepository->find($data['id']);

            if (!$entry) {
                return [
                    'code' => 3,
                    'message' =>$this->formCustomerRepository->getMessageByLang('notFoundData', 'Không tìm thấy kết quả'),
                ];
            }

            $entry->fill($data);

            if ($is_add_customer) {
                $entry->is_add_customer = true;
            }
            $entry->save();

            return [
                'code' => 0,
                'message' => $this->formCustomerRepository->getMessageByLang('updated', 'Đã cập nhật'),
                'id' => $entry->id
            ];
        }
    }


}
