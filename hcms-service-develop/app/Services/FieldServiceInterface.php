<?php

namespace App\Services;

interface FieldServiceInterface
{
    public function getFieldsForTable(string $table, $show = null, $orderBy = 'order_form', $root = false);
    public function getFilterForTable(string $table);
}
