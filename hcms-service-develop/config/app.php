<?php
return [
    'env' => env('APP_ENV', 'production'),
    'namespace' => env('APP_NAMESPACE', 'default'),
    'uri_prefix' => env('URI_PREFIX', ''),
    'debug' => env('APP_DEBUG', false),
    'url' => env('APP_URL', 'http://localhost:7012'),
    'image_url' => env('APP_IMAGE_URL', 'http://localhost:7011'),
    'jwt_secret' => env("JWT_SECRET"),
    'cache_enabled' => env('CACHE_ENABLED', true),
    'cache_driver' => env("CACHE_DRIVER", 'file'),
    'font_url' => env('CORS_FRONT_URL'),
    'avatar_url' => 'https://avatar.omicare.vn',
    'cors_origins' => [
        'http://localhost:8209' => true,
        'http://localhost:3000' => true,
        'http://localhost:8210' => true,
    ],
    'client' => [
    'secret' => env('HCMS_SERVICE_SECRET'),
    'secret_auth'=>env('SERVICE_SECRET_AUTH'),
    ],

];
